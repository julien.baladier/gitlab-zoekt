# How to enable LoadBalancer

By default, this helm chart uses a [headless service](https://kubernetes.io/docs/concepts/services-networking/service/#headless-services)
as the main entry point. If you want to use a load balancer in GCP, you need:

1. Obtain an internal IP in your GCP console ([VPC network / IP addresses](https://console.cloud.google.com/networking/addresses/list) -> Reserve internal static IP address)
1. Update your values file to include
  ```yaml
  service:
    type: LoadBalancer
    annotations:
      cloud.google.com/load-balancer-type: Internal
      networking.gke.io/internal-load-balancer-allow-global-access: "true"
    loadBalancerIP: <INTERNAL_IP>
  ```
1. Install or upgrade the helm chart
1. To verify that it works, you can execute
  ```
  kubectl exec gitlab-zoekt-0 -- curl -s -XPOST -d '{"Q":"gitaly"}' 'http://<INTERNAL_IP>:8080/api/search'
  ```