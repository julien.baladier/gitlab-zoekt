#!/usr/bin/env bash
SCRIPT_DIR="$( cd "$( dirname "$0" )" && pwd )"
source $SCRIPT_DIR/integration_fns.sh

CERT_MANAGER_VERSION='v1.11.0'
NAME='gitlab-zoekt'
DEFAULT_POD='gitlab-zoekt-0'

TIMEOUT="120s"

INDEXER_PORT=6065
WEBSERVER_PORT=6070
GATEWAY_PORT=8080
SECRET_NAME=gitlab-zoekt-basicauth
BASIC_USERNAME=gitlab
BASIC_PASSWORD=password123
SEARCH=zoekt
HELM_BASIC_AUTH="--set gateway.basicAuth.enabled=true --set gateway.basicAuth.secretName=$SECRET_NAME"
CURL="curl -v --max-time 90 --no-keepalive"
CURL_BASIC_AUTH="-u '$BASIC_USERNAME:$BASIC_PASSWORD'"

HELM_ARGS="$HELM_BASIC_AUTH ${@}"

function wait_gateway() {
  scheme=${1:-http}

  kubectl wait --for=condition=ready pod -l app.kubernetes.io/name=gitlab-zoekt-gateway --timeout=$TIMEOUT &> /dev/null
  cmd "Wait for gateway" wait_for "$DEFAULT_POD" "$scheme://$NAME-gateway:$GATEWAY_PORT/health"
}

function wait_statefulset() {
  scheme=${1:-http}

  kubectl wait --for=condition=ready pod -l app.kubernetes.io/name=gitlab-zoekt --timeout=$TIMEOUT &> /dev/null
  cmd "Wait for indexer" wait_for "$DEFAULT_POD" "http://127.0.0.1:$INDEXER_PORT/indexer/health"
  cmd "Wait for webserver" wait_for "$DEFAULT_POD" "http://127.0.0.1:$WEBSERVER_PORT/"
  cmd "Wait for external gateway" wait_for "$DEFAULT_POD" "$scheme://127.0.0.1:$GATEWAY_PORT/health"
}

function wait_pods() {
  wait_gateway "$1"
  wait_statefulset "$1"
}

# We preload the script into a function so that it's safe to edit the file while the script is being executed
function main() {
  start=$(date +%s)

  print_execution_mode

  cmd "Create basic_auth secret" "kubectl delete secret $SECRET_NAME --ignore-not-found=true && kubectl create secret generic $SECRET_NAME --from-literal=gitlab_username=$BASIC_USERNAME --from-literal=gitlab_password=$BASIC_PASSWORD"

  if [[ -z "${SKIP_INSTALL}" ]]; then
    echo "Installation:"
    cmd "Uninstall helm release if exists" "helm uninstall $NAME --wait || true"
    wait_cmd

    cmd "Helm install" helm install $NAME . --wait $HELM_ARGS
  else
    cmd "Helm upgrade --reset-values" helm upgrade $NAME . --wait --reset-values $HELM_ARGS
  fi

  if [[ -z "${SKIP_TLS}" ]]; then
    # We start the rollout early to save some time
    cmd "Deploy cert manager" kubectl apply -f "https://github.com/cert-manager/cert-manager/releases/download/$CERT_MANAGER_VERSION/cert-manager.yaml"
  fi

  echo "Healthchecks/Metrics & Indexing/Searching:"
  wait_statefulset

  wait_cmd
  cmd "Local | New indexer health" kubectl exec $DEFAULT_POD -- $CURL "http://127.0.0.1:$INDEXER_PORT/indexer/health"
  cmd "Local | Webserver health" kubectl exec $DEFAULT_POD -- $CURL "http://127.0.0.1:$WEBSERVER_PORT/"
  cmd "Internal Gateway | New indexer health" kubectl exec $DEFAULT_POD -- $CURL "http://127.0.0.1:$GATEWAY_PORT/indexer/health"
  cmd "Internal Gateway | New indexer metrics" kubectl exec $DEFAULT_POD -- $CURL "http://127.0.0.1:$GATEWAY_PORT/indexer/metrics"
  cmd "Internal Gateway | Webserver health" kubectl exec $DEFAULT_POD -- $CURL "http://127.0.0.1:$GATEWAY_PORT/"
  cmd "Internal Gateway | Webserver metrics" kubectl exec $DEFAULT_POD -- $CURL "http://127.0.0.1:$GATEWAY_PORT/"
  cmd "Internal Gateway | Nginx health" kubectl exec $DEFAULT_POD -- $CURL "http://127.0.0.1:$GATEWAY_PORT/health"

  wait_gateway
  wait_cmd
  cmd "External Gateway | Nginx health" kubectl exec $DEFAULT_POD -- $CURL "http://$NAME-gateway:$GATEWAY_PORT/health"
  cmd "External Gateway | Webserver health" kubectl exec $DEFAULT_POD -- $CURL "http://$NAME-gateway:$GATEWAY_PORT/"
  cmd "External Gateway | Webserver metrics" kubectl exec $DEFAULT_POD -- $CURL "http://$NAME-gateway:$GATEWAY_PORT/metrics"
  cmd "External Gateway | Indexer health" kubectl exec $DEFAULT_POD -- $CURL "http://$NAME-gateway:$GATEWAY_PORT/indexer/health"
  cmd "External Gateway | Indexer metrics" kubectl exec $DEFAULT_POD -- $CURL "http://$NAME-gateway:$GATEWAY_PORT/indexer/metrics"

  cmd "External Gateway | Wrong Auth | Indexer" "kubectl exec $DEFAULT_POD -- $CURL -u 'gitlab:badpassword' -d '{}' 'http://$NAME-gateway:$GATEWAY_PORT/indexer/index' | tee /dev/stderr | grep '401 Authorization Required'"
  cmd "External Gateway | Wrong Auth | Webserver" "kubectl exec $DEFAULT_POD -- $CURL -u 'gitlab:badpassword' -d '{\"Q\":\"$SEARCH\"}' 'http://$NAME-gateway:$GATEWAY_PORT/api/search' | tee /dev/stderr | grep '401 Authorization Required'"

  wait_cmd

  cmd "Local | Webserver" "kubectl exec $DEFAULT_POD -- $CURL $CURL_BASIC_AUTH -d '{\"Q\":\"$SEARCH\"}' 'http://127.0.0.1:$WEBSERVER_PORT/api/search' | tee /dev/stderr | grep 'FileCount'"
  cmd "Local | Indexer" "kubectl exec $DEFAULT_POD -- $CURL $CURL_BASIC_AUTH -d '{}' 'http://127.0.0.1:$INDEXER_PORT/indexer/index' | tee /dev/stderr | grep 'gitalyConnectionInfo'"
  cmd "Internal Gateway | Webserver" "kubectl exec $DEFAULT_POD -- $CURL $CURL_BASIC_AUTH -d '{\"Q\":\"$SEARCH\"}' 'http://127.0.0.1:$GATEWAY_PORT/api/search' | tee /dev/stderr | grep 'FileCount'"

  cmd "External Gateway | Indexer" "kubectl exec $DEFAULT_POD -- $CURL $CURL_BASIC_AUTH -d '{}' 'http://$NAME-gateway:$GATEWAY_PORT/indexer/index' | tee /dev/stderr | grep 'gitalyConnectionInfo'"
  cmd "External Gateway | Webserver" "kubectl exec $DEFAULT_POD -- $CURL $CURL_BASIC_AUTH -d '{\"Q\":\"$SEARCH\"}' 'http://$NAME-gateway:$GATEWAY_PORT/api/search' | tee /dev/stderr | grep 'FileCount'"
  cmd "External Gateway /nodes endpoint | Webserver" "kubectl exec $DEFAULT_POD -- $CURL $CURL_BASIC_AUTH -d '{\"Q\":\"$SEARCH\"}' 'http://$NAME-gateway:$GATEWAY_PORT/nodes/$DEFAULT_POD.$NAME.default.svc.cluster.local/api/search' | tee /dev/stderr | grep 'FileCount'"
  cmd "External Gateway /nodes endpoint | Indexer" "kubectl exec $DEFAULT_POD -- $CURL $CURL_BASIC_AUTH -d '{}' 'http://$NAME-gateway:$GATEWAY_PORT/nodes/$DEFAULT_POD.$NAME.default.svc.cluster.local/indexer/index' | tee /dev/stderr | grep 'gitalyConnectionInfo'"

  if [[ -z "${SKIP_TLS}" ]]; then
    cmd "Wait for cert-manager-webhook deployment" kubectl rollout status deployment cert-manager-webhook -n cert-manager --timeout=$TIMEOUT
    wait_cmd

    echo "TLS:"
    cmd "Add certificate" kubectl apply -f spec/k8s/cabootstrap.yaml
    wait_cmd

    cmd "Enable TLS" helm upgrade --set gateway.tls.certificate.enabled=true --set gateway.tls.certificate.create=true --set gateway.tls.certificate.issuer.name=my-ca-issuer --set gateway.tls.certificate.dnsNames='{zoekt.example.com}' $NAME . --wait $HELM_ARGS
    wait_pods "https"

    wait_cmd
    cmd "TLS | External Gateway | Webserver" "kubectl exec $DEFAULT_POD -- $CURL $CURL_BASIC_AUTH -d '{\"Q\":\"$SEARCH\"}' 'https://$NAME-gateway:$GATEWAY_PORT/api/search' -i -k | tee /dev/stderr | grep 'FileCount'"
    cmd "TLS | External Gateway | Indexer" "kubectl exec $DEFAULT_POD -- $CURL $CURL_BASIC_AUTH -d '{}' 'https://$NAME-gateway:$GATEWAY_PORT/indexer/index' -i -k | tee /dev/stderr | grep 'gitalyConnectionInfo'"
    cmd "TLS | External Gateway /nodes endpoint | Webserver" "kubectl exec $DEFAULT_POD -- $CURL $CURL_BASIC_AUTH -d '{\"Q\":\"$SEARCH\"}' 'https://$NAME-gateway:$GATEWAY_PORT/nodes/$DEFAULT_POD.$NAME.default.svc.cluster.local/api/search' -i -k | tee /dev/stderr | grep 'FileCount'"
    cmd "TLS | External Gateway /nodes endpoint | Indexer" "kubectl exec $DEFAULT_POD -- $CURL $CURL_BASIC_AUTH -d '{}' 'https://$NAME-gateway:$GATEWAY_PORT/nodes/$DEFAULT_POD.$NAME.default.svc.cluster.local/indexer/index' -i -k | tee /dev/stderr | grep 'gitalyConnectionInfo'"
    wait_cmd
  fi

  end=$(date +%s)
  echo "Elapsed Time: $(($end-$start)) seconds"
}

lock && main