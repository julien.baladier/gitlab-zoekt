require 'spec_helper'
require 'helm_template_helper'

describe 'templates/stateful_sets.yaml' do
  let(:path) { 'StatefulSet/test-gitlab-zoekt' }

  let(:default_values) do
    HelmTemplate.defaults
  end

  it 'creates the resource and adds labels', :aggregate_failures do
    t = HelmTemplate.new(default_values)
    expect(t.exit_code).to eq(0)

    expect(t.resource_exists?(path)).to eq(true)
    expect(t.labels(path)).to include('app.kubernetes.io/name' => 'gitlab-zoekt')
    expect(t.labels(path)).to include('app.kubernetes.io/instance' => 'test')
    expect(t.labels(path)).to have_key('helm.sh/chart')
    expect(t.labels(path)).to have_key('app.kubernetes.io/instance')
    expect(t.labels(path)).to have_key('app.kubernetes.io/version')
    expect(t.template_labels(path)).to include('app.kubernetes.io/name' => 'gitlab-zoekt')
    expect(t.template_labels(path)).to include('app.kubernetes.io/instance' => 'test')
  end

  it 'disables ctags' do
    t = HelmTemplate.new(default_values)

    expect(t.exit_code).to eq(0)
    expect(t.env(path, 'zoekt-indexer')).to include({ "name" => "CTAGS_COMMAND", "value" => "" })
  end

  it_behaves_like 'builds gateway image', 'zoekt-internal-gateway'

  describe 'gateway.basicAuth.enabled' do
    let(:values) do
      YAML.safe_load(%(
        gateway:
          basicAuth:
            enabled: #{basic_auth}
      )).merge(default_values)
    end

    context 'when true' do
      let(:basic_auth) { true }

      it 'adds an initContainer for create-htpasswd' do
        t = HelmTemplate.new(values)
        expect(t.exit_code).to eq(0)

        expect(t.find_container(path, 'create-htpasswd', true)).to be_a(Hash)
      end
    end

    context 'when false' do
      let(:basic_auth) { false }

      it 'does not add an initContainer for create-htpasswd' do
        t = HelmTemplate.new(values)
        expect(t.exit_code).to eq(0)

        expect(t.find_container(path, 'create-htpasswd', true)).to be_nil
      end
    end
  end

  describe 'gateway.tls.certificate.enabled' do
    let(:values) do
      YAML.safe_load(%(
        gateway:
          tls:
            certificate:
              enabled: #{tls_certificate}
      )).merge(default_values)
    end

    context 'when true' do
      let(:tls_certificate) { true }

      it 'uses HTTPS scheme for livenessProbe' do
        t = HelmTemplate.new(values)
        expect(t.exit_code).to eq(0)

        container = t.find_container(path, 'zoekt-internal-gateway')
        expect(container.dig('livenessProbe', 'httpGet', 'scheme')).to eq('HTTPS')
      end

      it 'uses HTTPS scheme for readinessProbe' do
        t = HelmTemplate.new(values)
        expect(t.exit_code).to eq(0)

        container = t.find_container(path, 'zoekt-internal-gateway')
        expect(container.dig('readinessProbe', 'httpGet', 'scheme')).to eq('HTTPS')
      end

      it 'adds a volumeMounts for gateway-cert to zoekt-internal-gateway container' do
        t = HelmTemplate.new(values)
        expect(t.exit_code).to eq(0)

        volume_mounts = t.find_volume_mounts(path, 'zoekt-internal-gateway', 'gateway-cert')

        expect(volume_mounts).to be_a(Array)
        expect(volume_mounts).to include({
          "name" => "gateway-cert",
          "mountPath" => "/etc/ssl/tls.crt",
          "subPath" => "tls.crt",
          "readOnly" => true
        }
        )
        expect(volume_mounts).to include({
          "name" => "gateway-cert",
          "mountPath" => "/etc/ssl/tls.key",
          "subPath" => "tls.key",
          "readOnly" => true
        }
        )
      end

      it 'adds a volume for gateway-cert to zoekt-internal-gateway container', :aggregate_failures do
        t = HelmTemplate.new(values)
        expect(t.exit_code).to eq(0)

        volume = t.find_volume(path, 'gateway-cert')
        expect(volume).not_to be_nil

        secret = volume.dig('secret')
        expect(secret.dig('secretName')).to eq('zoekt-gateway-cert')
        expect(secret.dig('items')).to include({ "key" => "tls.crt", "path" => "tls.crt" })
        expect(secret.dig('items')).to include({ "key" => "tls.key", "path" => "tls.key" })
      end
    end

    context 'when false' do
      let(:tls_certificate) { false }

      it 'does not add a scheme for livenessProbe' do
        t = HelmTemplate.new(values)
        expect(t.exit_code).to eq(0)

        container = t.find_container(path, 'zoekt-internal-gateway')
        expect(container.dig('livenessProbe', 'httpGet', 'scheme')).to be_nil
      end

      it 'does not add a scheme for readinessProbe' do
        t = HelmTemplate.new(values)
        expect(t.exit_code).to eq(0)

        container = t.find_container(path, 'zoekt-internal-gateway')
        expect(container.dig('readinessProbe', 'httpGet', 'scheme')).to be_nil
      end

      it 'does not add a volumeMount for gateway-cert to zoekt-internal-gateway container' do
        t = HelmTemplate.new(values)
        expect(t.exit_code).to eq(0)

        expect(t.find_volume_mount(path, 'zoekt-internal-gateway', 'gateway-cert')).to be_nil
      end
    end
  end

  describe 'indexer.internalApi.enabled' do
    let(:values) do
      YAML.safe_load(%(
        indexer:
          internalApi:
            enabled: #{internal_api}
            gitlabUrl: 'https://gitlab.example.com'
      )).merge(default_values)
    end

    context 'when true' do
      let(:internal_api) { true }

      it 'adds a volumeMount for internal-api-secret to zoekt-indexer container' do
        t = HelmTemplate.new(values)
        expect(t.exit_code).to eq(0)

        volume_mount = t.find_volume_mount(path, 'zoekt-indexer', 'internal-api-secret')

        expect(volume_mount.dig('readOnly')).to eq(true)
        expect(volume_mount.dig('mountPath')).to eq('/.gitlab_shell_secret')
        expect(volume_mount.dig('subPath')).to eq('.gitlab_shell_secret')
      end

      it 'adds a volume for internal-api-secret to zoekt-indexer container' do
        t = HelmTemplate.new(values)
        expect(t.exit_code).to eq(0)

        volume = t.find_volume(path, 'internal-api-secret')
        expect(volume.dig('name')).to eq('internal-api-secret')

        secret = volume.dig('secret')
        expect(secret.dig('secretName')).to eq('test-internal-api-secret-name')
        expect(secret.dig('items')).to include({ "key" => "test-internal-api-secret-key", "path" => ".gitlab_shell_secret" })
      end

      it 'adds GITLAB_URL and SERVICE_URL environment variables' do
        t = HelmTemplate.new(values)
        expect(t.exit_code).to eq(0)

        zoekt_indexer_env = t.env(path, 'zoekt-indexer')
        expect(zoekt_indexer_env).to include(
          { "name" => "SERVICE_URL", "value" => "http://test-gitlab-zoekt-gateway.default.svc.cluster.local:8080" }
        )

        expect(zoekt_indexer_env).to include(
          { "name" => "GITLAB_URL", "value" => 'https://gitlab.example.com' }
        )
      end
    end

    context 'when false' do
      let(:internal_api) { false }

      it 'does not add a volumeMount for internal-api-secret to zoekt-indexer container' do
        t = HelmTemplate.new(values)
        expect(t.exit_code).to eq(0)

        expect(t.find_volume_mount(path, 'zoekt-indexer', 'internal-api-secret')).to be_nil
      end

      it 'does not add a volume for internal-api-secret to zoekt-indexer container' do
        t = HelmTemplate.new(values)
        expect(t.exit_code).to eq(0)

        expect(t.find_volume(path, 'internal-api-secret')).to be_nil
      end

      it 'does not add GITLAB_URL and SERVICE_URL environment variables' do
        t = HelmTemplate.new(values)
        expect(t.exit_code).to eq(0)

        expect(t.env(path, 'zoekt-indexer')).not_to include(hash_including("name" => "SERVICE_URL"))
        expect(t.env(path, 'zoekt-indexer')).not_to include(hash_including("name" => "GITLAB_URL"))
      end
    end
  end

  describe 'indexer.googleCloudProfiler.enabled' do
    let(:values) do
      YAML.safe_load(%(
        indexer:
          googleCloudProfiler:
            enabled: #{google_cloud_profiler}
      )).merge(default_values)
    end

    context 'when true' do
      let(:google_cloud_profiler) { true }

      it 'adds GOOGLE_CLOUD_PROFILER_ENABLED environment variable' do
        t = HelmTemplate.new(values)
        expect(t.exit_code).to eq(0)

        zoekt_indexer_env = t.env(path, 'zoekt-indexer')
        expect(zoekt_indexer_env).to include(
          { "name" => "GOOGLE_CLOUD_PROFILER_ENABLED", "value" => "true" }
        )
      end
    end

    context 'when false' do
      let(:google_cloud_profiler) { false }

      it 'does not add an initContainer for create-htpasswd' do
        t = HelmTemplate.new(values)
        expect(t.exit_code).to eq(0)

        expect(t.env(path, 'zoekt-indexer')).not_to include(hash_including("name" => "GOOGLE_CLOUD_PROFILER_ENABLED"))
      end
    end
  end

  describe 'webserver.environment' do
    let(:container_name) { 'zoekt-webserver' }

    context 'when set' do
      let(:gogc_value) { 75 }
      let(:gomaxprocs_value) { 4 }
      let(:values) do
        YAML.safe_load(%(
          webserver:
            environment:
              GOGC: #{gogc_value}
              GOMAXPROCS: #{gomaxprocs_value}
        )).merge(default_values)
      end

      it 'adds GOGC environment variable' do
        t = HelmTemplate.new(values)
        expect(t.exit_code).to eq(0)

        container_env = t.env(path, container_name)
        expect(container_env).to include(
          { "name" => "GOGC", "value" => gogc_value.to_s }
        )
        expect(container_env).to include(
          { "name" => "GOMAXPROCS", "value" => gomaxprocs_value.to_s }
        )
      end
    end

    context 'when empty' do
      let(:values) { default_values }

      it 'does not add environment variables' do
        t = HelmTemplate.new(values)
        expect(t.exit_code).to eq(0)

        expect(t.env(path, container_name)).to be_nil
      end
    end
  end

  describe 'indexer.environment' do
    let(:container_name) { 'zoekt-indexer' }

    context 'when set' do
      let(:gogc_value) { 75 }
      let(:gomaxprocs_value) { 4 }
      let(:values) do
        YAML.safe_load(%(
          indexer:
            environment:
              GOGC: #{gogc_value}
              GOMAXPROCS: #{gomaxprocs_value}
        )).merge(default_values)
      end

      it 'adds environment variables' do
        t = HelmTemplate.new(values)
        expect(t.exit_code).to eq(0)

        container_env = t.env(path, container_name)
        expect(container_env).to include(
          { "name" => "GOGC", "value" => gogc_value.to_s }
        )
        expect(container_env).to include(
          { "name" => "GOMAXPROCS", "value" => gomaxprocs_value.to_s }
        )
      end
    end

    context 'when empty' do
      let(:values) { default_values }

      it 'does not add GOGC environment variable' do
        t = HelmTemplate.new(values)
        expect(t.exit_code).to eq(0)

        container_env = t.env(path, container_name)
        expect(container_env).not_to include(hash_including("name" => "GOGC"))
        expect(container_env).not_to include(hash_including("name" => "GOMAXPROCS"))
      end
    end
  end

end
