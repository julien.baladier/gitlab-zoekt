require 'spec_helper'
require 'helm_template_helper'

describe 'templates/deployment-nginx-configmap.yaml' do
  let(:path) { 'ConfigMap/test-gitlab-zoekt-gateway-nginx-conf' }

  let(:default_values) do
    HelmTemplate.defaults
  end

  it 'creates the resource and adds labels', :aggregate_failures do
    t = HelmTemplate.new(default_values)
    expect(t.exit_code).to eq(0)

    expect(t.resource_exists?(path)).to eq(true)
    expect(t.labels(path)).to include('app.kubernetes.io/name' => 'gitlab-zoekt')
    expect(t.labels(path)).to include('app.kubernetes.io/instance' => 'test')
    expect(t.labels(path)).to have_key('helm.sh/chart')
    expect(t.labels(path)).to have_key('app.kubernetes.io/instance')
    expect(t.labels(path)).to have_key('app.kubernetes.io/version')
  end

  describe 'gateway.tls.certificate.enabled' do
    let(:values) do
      YAML.safe_load(%(
        gateway:
          tls:
            certificate:
              enabled: #{tls_enabled}
      )).merge(default_values)
    end

    context 'when true' do
      let(:tls_enabled) { true }

      it 'adds ssl certificate to nginx.conf' do
        t = HelmTemplate.new(values)
        expect(t.exit_code).to eq(0)

        nginx_conf = t.dig(path, 'data', 'nginx.conf')

        expect(nginx_conf).to include('listen       8080 ssl;')
        expect(nginx_conf).to include('ssl_certificate')
        expect(nginx_conf).to include('ssl_certificate_key')
        expect(nginx_conf).to include('ssl_protocols TLS')
      end
    end

    context 'when false' do
      let(:tls_enabled) { false }

      it 'does not adds the ssl certificate to nginx.conf' do
        t = HelmTemplate.new(values)
        expect(t.exit_code).to eq(0)

        nginx_conf = t.dig(path, 'data', 'nginx.conf')

        expect(nginx_conf).to include('listen       8080;')
        expect(nginx_conf).not_to include('listen       8080 ssl;')
        expect(nginx_conf).not_to include('ssl_certificate')
        expect(nginx_conf).not_to include('ssl_certificate_key')
        expect(nginx_conf).not_to include('ssl_protocols TLS')
      end
    end
    # With/without annotations for all the things
  end
end