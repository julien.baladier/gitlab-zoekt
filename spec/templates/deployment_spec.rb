require 'spec_helper'
require 'helm_template_helper'

describe 'templates/deployment.yaml' do
  let(:path) { 'Deployment/test-gitlab-zoekt-gateway' }

  let(:default_values) do
    HelmTemplate.defaults
  end

  it 'includes the defaults' do
    t = HelmTemplate.new(default_values)

    expect(t.exit_code).to eq(0)
    expect(t.resource_exists?(path)).to eq(true)
    expect(t.labels(path)).to include('app.kubernetes.io/name' => 'gitlab-zoekt')
    expect(t.labels(path)).to include('app.kubernetes.io/instance' => 'test')
    expect(t.labels(path)).to have_key('helm.sh/chart')
    expect(t.labels(path)).to have_key('app.kubernetes.io/instance')
    expect(t.labels(path)).to have_key('app.kubernetes.io/version')
    expect(t.annotations(path)).to be_nil
  end

  it_behaves_like 'builds gateway image', 'zoekt-external-gateway'

  context 'when deploymentAnnotations has multiple values' do
    let(:values) do
      YAML.safe_load(%(
        deploymentAnnotations:
          secret.reloader.stakater.com/reload: some-cert
          foo: bar
      )).merge(default_values)
    end

    it 'includes the annotations' do
      t = HelmTemplate.new(values)

      expect(t.exit_code).to eq(0)
      expect(t.resource_exists?(path)).to eq(true)
      expect(t.annotations(path)).to include('secret.reloader.stakater.com/reload' => 'some-cert', 'foo' => 'bar')
    end
  end
end
