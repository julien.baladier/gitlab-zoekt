RSpec.shared_examples 'builds gateway image' do |container_name|
  describe 'image' do
    context 'when only image tag is set' do
      let(:values) do
        YAML.safe_load(%(
          gateway:
            image:
              digest: 
              tag: foo_bar_tag
        )).merge(default_values)
      end
  
      it 'sets the image to the tag' do
        t = HelmTemplate.new(values)
  
        expect(t.exit_code).to eq(0)
        expect(t.resource_exists?(path)).to eq(true)
        expect(t.find_container(path, container_name)).to include('image' => 'nginx:foo_bar_tag')
      end
    end

    context 'when only image digest is set' do
      let(:values) do
        YAML.safe_load(%(
          gateway:
            image:
              digest: foo_bar_digest
        )).merge(default_values)
      end
  
      it 'sets the image to the digest' do
        t = HelmTemplate.new(values)
  
        expect(t.exit_code).to eq(0)
        expect(t.resource_exists?(path)).to eq(true)
        expect(t.find_container(path, container_name)).to include('image' => 'nginx@foo_bar_digest')
      end
    end

    context 'when image digest and tag are both set' do
      let(:values) do
        YAML.safe_load(%(
          gateway:
            image:
              digest: foo_bar_digest
              tag: foo_bar_tag
        )).merge(default_values)
      end
  
      it 'sets the image to the digest' do
        t = HelmTemplate.new(values)
  
        expect(t.exit_code).to eq(0)
        expect(t.resource_exists?(path)).to eq(true)
        expect(t.find_container(path, container_name)).to include('image' => 'nginx@foo_bar_digest')
      end
    end
  end

end